# README

TT_Lib² is a library of shared functionality for my [SketchUp extensions](http://extensions.sketchup.com/users/thomthom). It is distributed as a [standalone package](http://extensions.sketchup.com/content/tt_lib%C2%B2) which most of my extensions depend upon to be installed.


## Documentation

http://www.thomthom.net/software/sketchup/tt_lib2/doc/


## License

The library is licensed under the MIT license with the exception of Daniel Berger's Win32 API library which is under the Artistic 2.0 license.

If you use code from this repository, please ensure you change the root namespace module so it doesn't clash with my original library.


## Development

### Ruby C Extension

#### Build Release

Windows: Use Visual Studio's batch build feature.

OSX:

    cd cext/tt_lib2
    ./build.sh

#### Install Binaries

Windows:

    install_cext.bat

OSX:

    ./install_cext.sh

