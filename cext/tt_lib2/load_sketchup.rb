# Make sure to load the Ruby Console so we can see load errors and other debug
# information.
SKETCHUP_CONSOLE.show


SOLUTION_PATH = File.expand_path(File.dirname(__FILE__))

ROOT_PATH = File.expand_path('../..', SOLUTION_PATH)
EXTENSION_SOURCE_PATH = ROOT_PATH


# Determine which build is being loaded and load the appropriate binaries from
# the build directory.
POINTER_SIZE = ['a'].pack('P').size * 8

# Currently the values are:
# * "DevelopmentBuild:Debug (2.2)"
# * "DevelopmentBuild:Release (2.2)"
startup_args = ARGV[0]
devbuild_id, build_configuration = startup_args.split(":")

BUILD_CONFIGURATION = build_configuration.freeze

require "sketchup.rb"
if POINTER_SIZE == 64
  require_relative "x64/#{BUILD_CONFIGURATION}/tt_lib2"
else
  require_relative "#{BUILD_CONFIGURATION}/tt_lib2"
end


# Load the extension registration. Then forcefully load the extension in case
# the extension has been disabled.
$LOAD_PATH << EXTENSION_SOURCE_PATH
require "TT_Lib2.rb"


# Output some debug info to the console so it's easier to verify what version
# of the extension actually is built and loaded.
puts "Loaded TT_Lib (#{BUILD_CONFIGURATION} #{POINTER_SIZE}bit)"
puts "> Version: #{TT::Lib::VERSION}"
puts "> CEXT Version: #{TT::CEXT_VERSION}"
