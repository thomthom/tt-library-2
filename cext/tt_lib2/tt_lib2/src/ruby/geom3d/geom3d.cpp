#include "geom3d.h"

#include "src/ruby/utilities.h"
#include "src/ruby/geom3d/bezier.h"


namespace ruby {
namespace geom3d {


void InitModule(VALUE mTT)
{
  VALUE mGeom3d = rb_define_module_under(mTT, "Geom3d");

  bezier::InitModule(mGeom3d);
}


} // namespace geom3d
} // namespace ruby
