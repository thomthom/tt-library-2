#pragma once

#include "ruby/framework.h"


namespace ruby {
namespace geom3d {
namespace bezier {


void InitModule(VALUE parent);


} // namespace bezier
} // namespace geom3d
} // namespace ruby
