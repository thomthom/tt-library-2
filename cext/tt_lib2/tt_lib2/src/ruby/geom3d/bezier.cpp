#include "bezier.h"

#include <vector>

#include "geometry/bezier.h"
#include "geometry/point3d.h"

#include "ruby/utilities.h"

#include "ruby/api/geom/point3d.h"


namespace ruby {
namespace geom3d {
namespace bezier {


namespace {

#pragma warning (push)
#pragma warning (disable : 4100) // warning C4100: unreferenced formal parameter

///// Function Macros //////////////////////////////////////////////////////////

#define PT2SKP(p) point_to_sketchup(p)
#define SKP2PT(p) sketchup_to_point(p)

// Mutes the warnings when -Wconversion is enabled.
#define ALLOC_N_UINT(type, n) (ALLOC_N( type, (unsigned int) n ))


///// Structs //////////////////////////////////////////////////////////////////

typedef struct Point3d Point3d;

struct Point3d {
  double x, y, z;
};


///// Prototypes ///////////////////////////////////////////////////////////////

inline VALUE point_to_sketchup(Point3d point);

inline Point3d sketchup_to_point(VALUE skp_point);

inline VALUE points_to_sketchup(Point3d points[], long nPoints);

inline void sketchup_to_points(VALUE skp_points, Point3d c_points[],
  long nPoints);

VALUE wrap_eval(VALUE self, VALUE skp_points, VALUE position);

VALUE wrap_curve( VALUE self, VALUE skp_points, VALUE subdivs);

VALUE wrap_patch( VALUE self, VALUE skp_points, VALUE subdivs);

Point3d bezier_eval(Point3d points[], long nPoints, double t);

int bezier_curve(Point3d points[], long nPoints, Point3d curve[], long nCurve);

unsigned long isqrt(unsigned long x);

static int is_integer_perfect_square(long number);


///// Global References ////

static VALUE mGeom;     // module Geom
static VALUE cPoint3d;  //  class Geom::Point3d

static ID iX;
static ID iY;
static ID iZ;

static ID iNew;


///// Ruby Extension Methods /////

/**
 * @param [Array<Geom::Point3d>] skp_points
 * @param [Float] position of curve defined in the range of 0.0 to 1.0
 *
 * @return [Geom::Point3d]
 */
VALUE wrap_eval(VALUE self, VALUE skp_points, VALUE position)
{
  long nPoints;
  double t;
  Point3d *c_points = NULL;
  Point3d c_point;

  Check_Type( skp_points, T_ARRAY );
  nPoints = RARRAY_LEN( skp_points );
  t = NUM2DBL( position );

  if ( nPoints < 2 )
    rb_raise( rb_eArgError, "Must have at least two control points." );

  if ( t < 0.0 || t > 1.0 )
    rb_raise( rb_eArgError, "Position must be between 0.0 and 1.0." );

  c_points = ALLOC_N_UINT( Point3d, nPoints );

  sketchup_to_points( skp_points, c_points, nPoints );
  c_point = bezier_eval( c_points, nPoints, t );

  xfree( c_points );
  c_points = NULL;

  return point_to_sketchup( c_point );
}


/*
 * @param [Array<Geom::Point3d>] skp_points Must be more than two points.
 * @param [Integer] subdivs Must be higher than zero.
 *
 * @return [Array<Geom::Point3d>]
 */
VALUE wrap_curve( VALUE self, VALUE skp_points, VALUE subdivs )
{
  int nSubdivs;
  long nPoints, nCurve;
  Point3d *c_curve = NULL;
  Point3d *c_points = NULL;
  VALUE skp_curve;
  
  Check_Type( skp_points, T_ARRAY );
  nSubdivs = NUM2INT( subdivs );
  nPoints = RARRAY_LEN( skp_points );
  nCurve = nSubdivs + 1;
  
  if ( nSubdivs < 1 )
    rb_raise( rb_eArgError, "Subdivisions must be at least one." );
    
  if ( nPoints < 2 )
    rb_raise( rb_eArgError, "Must have at least two control points." );
  
  c_points = ALLOC_N_UINT( Point3d, nPoints );
  c_curve  = ALLOC_N_UINT( Point3d, nCurve );

  sketchup_to_points( skp_points, c_points, nPoints );
  if ( bezier_curve( c_points, nPoints, c_curve, nCurve ) )
    rb_raise( rb_eStandardError, "Could not create bezier curve." ); // (?)
  skp_curve = points_to_sketchup( c_curve, nCurve );
  
  xfree( c_curve );
  c_curve = NULL;
  
  xfree( c_points );
  c_points = NULL;
  
  return skp_curve;
}


/*
 * @param [Array<Geom::Point3d>] ruby_points Must be more than four points.
 * @param [Integer] subdivs Must be larger than zero.
 *
 * @return [Array<Geom::Point3d>]
 */
VALUE wrap_patch(VALUE self, VALUE ruby_points, VALUE subdivs)
{
	using namespace tt_lib2::ruby::api;
	using namespace tt_lib2::geometry::bezier;

	Check_Type(ruby_points, T_ARRAY);
	auto segments = NUM2INT(subdivs);
	auto num_points = RARRAY_LEN(ruby_points);

	if (segments < 0)
		rb_raise(rb_eArgError, "Subdivisions must be larger than zero.");

	if (num_points < 4)
		rb_raise(rb_eArgError, "Must have at least four control points.");

	if (!is_integer_perfect_square(num_points))
		rb_raise(rb_eArgError, "Number of points must be a perfect square.");

	auto control_points = Geom::VALUE2Points(ruby_points);
	auto bezier_patch_points = QuadPatch(control_points, segments);
	return Geom::GetVALUE(bezier_patch_points);
}


///// C Functions /////


/*
  No validation is performed by this function - that is done by the SketchUp
  wrapper methods calling this function.

  `t` is the position of the curve given in a value between 0.0 and 1.0.
 */
Point3d bezier_eval( Point3d points[], long nPoints, double t )
{
  long i;
  long degree, n_choose_i;
  double t1, fact, fn;
  double x, y, z;
  Point3d result;

  degree = nPoints - 1; // Must be at least 1 or higher.
  
  t1 = 1.0 - t;
  fact = 1.0;
  n_choose_i = 1;

  x = points[0].x * t1;
  y = points[0].y * t1;
  z = points[0].z * t1;

  for ( i = 1; i < degree; i++ )
  {
    fact = fact * t;
    n_choose_i = n_choose_i * ( degree - i + 1) / i;
    fn = fact * n_choose_i;
    x = ( x + fn * points[i].x ) * t1;
    y = ( y + fn * points[i].y ) * t1;
    z = ( z + fn * points[i].z ) * t1;
  }

  result.x = x + fact * t * points[degree].x;
  result.y = y + fact * t * points[degree].y;
  result.z = z + fact * t * points[degree].z;

  return result;
}


int bezier_curve( Point3d points[], long nPoints, Point3d curve[], long nCurve )
{
  double dt, t;
  long subdivs;
  long i;
  
  if ( nCurve < 2 )
    return 1;
    
  if ( nPoints < 2 )
    return 2;
  
  subdivs = nCurve - 1;
  dt = 1.0 / (double) subdivs;
  for ( i = 0; i <= subdivs; i++ )
  {
    t = (double) i * dt;
    curve[i] = bezier_eval( points, nPoints, t );
  }
  
  return 0;
}


/* Integer Square Root
 *
 * http://www.codecodex.com/wiki/Calculate_an_integer_square_root#C
 */
unsigned long isqrt( unsigned long x )
{
  unsigned long op, res, one;

  op = x;
  res = 0;

  /* "one" starts at the highest power of four <= than the argument. */
  one = 1 << 30;  /* second-to-top bit set */
  while (one > op) one >>= 2;

  while (one != 0) {
    if (op >= res + one) {
      op -= res + one;
      res += one << 1;  // <-- faster than 2 * one
    }
    res >>= 1;
    one >>= 2;
  }
  return res;
}


/* Is Integer Perfect Square?
 *
 */
int is_integer_perfect_square( long number )
{
  // http://www.convertdatatypes.com/Convert-int-to-ulong-in-C.html
  unsigned long s, n;
  n = (unsigned long) number;
  s = isqrt( n );
  return ( n == s * s ) ? 1 : 0;
}


///// C <-> Ruby Bridges /////


/*
  Converts a struct Point3d to a ruby Geom::Point3d.
  
  Macro: PT2SKP
 */
inline VALUE point_to_sketchup( Point3d point )
{
  VALUE skp_point;

  skp_point = rb_funcall( cPoint3d, iNew, 3,
    DBL2NUM( point.x ),
    DBL2NUM( point.y ),
    DBL2NUM( point.z )
  );
  
  return skp_point;
}


/*
  Converts a ruby Geom::Point3d to a struct Point3d.
  
  Macro: SKP2PT
 */
inline Point3d sketchup_to_point( VALUE skp_point )
{
  Point3d point;

  point.x = NUM2DBL( rb_funcall(skp_point, iX, 0) );
  point.y = NUM2DBL( rb_funcall(skp_point, iY, 0) );
  point.z = NUM2DBL( rb_funcall(skp_point, iZ, 0) );
  
  return point;
}


inline VALUE points_to_sketchup( Point3d points[], long nPoints )
{
  long i;
  VALUE skp_points, skp_point;

  skp_points = rb_ary_new();
  for ( i = 0; i < nPoints; i++ )
  {
    skp_point = point_to_sketchup( points[i] );
    rb_ary_push( skp_points, skp_point );
  }

  return skp_points;
}


inline void sketchup_to_points( VALUE skp_points, Point3d c_points[], long nPoints )
{
  long i;
  VALUE point;
  
  for ( i = 0; i < nPoints; i++ )
  {
    point = rb_ary_entry( skp_points, i );
    c_points[i] = sketchup_to_point( point );
  }

  return;
}

#pragma warning (pop)

} // namespace


/*
 * Document-module: TT::Bezier
 *
 * Methods to evaluate various bezier functions.
 */


/*
 * Document-module: Geom
 *
 * @private
 */


void InitModule(VALUE mTT)
{
  // SketchUp API References
  iX = rb_intern("x");
  iY = rb_intern("y");
  iZ = rb_intern("z");
  
  iNew = rb_intern("new");

  mGeom    = rb_define_module("Geom");
  cPoint3d = rb_define_class_under(mGeom, "Point3d", rb_cObject);

  // Modules
  VALUE mBezier = rb_define_module_under(mTT, "Bezier");

  // Bezier kernel version.
  rb_define_const(mBezier, "CEXT_VERSION", GetVALUE("2.0.0"));

  // Methods
  // > TT::Geom3d::Bezier.eval( points, t )
  rb_define_module_function(mBezier, "eval", VALUEFUNC(wrap_eval), 2);
  // > TT::Geom3d::Bezier.curve( points, subdivs )
  rb_define_module_function(mBezier, "curve", VALUEFUNC(wrap_curve), 2);
  rb_define_alias(rb_singleton_class(mBezier), "points", "curve");
  // > TT::Geom3d::Bezier.patch( points, subdivs )
  rb_define_module_function(mBezier, "patch", VALUEFUNC(wrap_patch), 2);
}


} // namespace bezier
} // namespace geom3d
} // namespace ruby
