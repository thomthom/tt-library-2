#pragma once

#include "src/ruby/framework.h"


namespace ruby {
namespace geom3d {


void InitModule(VALUE parent);


} // namespace geom3d
} // namespace ruby
