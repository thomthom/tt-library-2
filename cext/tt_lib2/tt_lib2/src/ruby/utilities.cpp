#include "utilities.h"


VALUE GetVALUE(const char* string) {
  VALUE ruby_string = rb_str_new2(string);
#ifdef HAVE_RUBY_ENCODING_H
  // Mark all strings as UTF-8 encoded Ruby 2.0 generally expects strings to be
  // Encoded UTF-8.
  static int enc_index = rb_enc_find_index("UTF-8");
  rb_enc_associate_index(ruby_string, enc_index);
#endif
  return ruby_string;
}

VALUE GetVALUE(const std::string string)
{
  return GetVALUE(string.c_str());
}
