#pragma once

#include <string>
#include <set>
#include <vector>


namespace ruby {
namespace system {

typedef std::vector<std::string> FontList;

struct FontOptions
{
  bool device;
  bool raster;
  bool vector;
};

FontList GetFontNames(const FontOptions& options);


} // namespace system
} // namespace ruby
