#include "window.h"

#include <assert.h>


namespace tt_lib2 {
namespace ui {
namespace win32 {

namespace {


BOOL CALLBACK EnumThreadWndProc(HWND hwnd, LPARAM lParam)
{
  // TODO: Might want to check the window title of `root` to ensure that is
  //       really is the SketchUp Window. When going over the whole set of
  //       windows it appear to be some where the root isn't SketchUp.
  HWND root = GetAncestor(hwnd, GA_ROOTOWNER);
  assert(root);
  assert(lParam);
  HWND* hwnd_out = reinterpret_cast<HWND*>(lParam);
  assert(hwnd_out);
  *hwnd_out = root;
  return FALSE;
}


} // namespace


HWND GetSketchUpWindow()
{
  // TODO: Review is there is a better way of getting a handle to the
  //       SketchUp window.
  //
  // GetActiveWindow cannot be used because it fails when no SketchUp windows
  // are active. This function might be called when SketchUp is not the active
  // window because it's doing some long task that made the user switch to do
  // something else.
  // Instead the thread's windows are enumerated and the root owner is picked.
  // This should be the SketchUp window.
  //
  // TODO: Can the result be cached? Does this have an performance impact of
  //       looking up every time?
  DWORD thread_id = GetCurrentThreadId();
  assert(thread_id);
  HWND sketchup_handle = nullptr;
  LPARAM lParam = reinterpret_cast<LPARAM>(&sketchup_handle);
  EnumThreadWindows(thread_id, EnumThreadWndProc, lParam);
  assert(sketchup_handle);
  return sketchup_handle;
}


} // namespace win32
} // namespace ui
} // namespace tt_lib2
