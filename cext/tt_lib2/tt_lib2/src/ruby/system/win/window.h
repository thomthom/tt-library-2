#pragma once

#ifndef UNICODE
#define UNICODE
#endif
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>


namespace tt_lib2 {
namespace ui {
namespace win32 {


HWND GetSketchUpWindow();


} // namespace win32
} // namespace ui
} // namespace tt_lib2