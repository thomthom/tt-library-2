#include "../fonts.h"

#include <algorithm>
#include <codecvt>
#include <locale>

#ifndef UNICODE
#define UNICODE
#endif
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include "ruby/system/win/window.h"


namespace ruby {
namespace system {
namespace {

struct FontData
{
  DWORD type_filter;
  FontList& names;
};

#pragma warning (push)
#pragma warning (disable : 4100) // warning C4100: unreferenced formal parameter
int CALLBACK GetFontsCallback(
  const LOGFONT* lpelfe,
  const TEXTMETRIC* lpntme,
  DWORD FontType,
  LPARAM lParam)
{
  auto params = reinterpret_cast<FontData*>(lParam);

  if (FontType & params->type_filter)
  {
    // If in the future more info is needed, cast to ENUMLOGFONTEX*.
    //auto lf = reinterpret_cast<const ENUMLOGFONTEX*>(lpelfe);

    std::wstring utf16_name = lpelfe->lfFaceName;
    std::wstring_convert<std::codecvt_utf8<wchar_t>> utf8_conv;
    auto utf8_name = utf8_conv.to_bytes(utf16_name);

    // Filter out font names for vertical printing.
    // https://blogs.msdn.microsoft.com/oldnewthing/20120719-00/?p=7093/
    if (utf8_name[0] != '@')
    {
      params->names.emplace_back(utf8_name);
    }
  }
  return TRUE;
}
#pragma warning (pop)

} // namespace

FontList GetFontNames(const FontOptions& options)
{
  DWORD type_filter = 0;
  if (options.device) type_filter |= DEVICE_FONTTYPE;
  if (options.raster) type_filter |= RASTER_FONTTYPE;
  if (options.vector) type_filter |= TRUETYPE_FONTTYPE;

  FontList fonts;
  FontData font_data { type_filter, fonts };

  if (type_filter == 0)
    return fonts;

  LOGFONT log_font {};
  log_font.lfCharSet = DEFAULT_CHARSET;
  log_font.lfFaceName[0] = '\0';
  log_font.lfPitchAndFamily = 0;

  HWND hwnd = tt_lib2::ui::win32::GetSketchUpWindow();
  HDC hdc = ::GetDC(hwnd);
  EnumFontFamiliesEx(hdc, &log_font, GetFontsCallback,
      reinterpret_cast<LPARAM>(&font_data), 0);
  ::ReleaseDC(nullptr, hdc);

  // EnumFontFamiliesEx will return the same name multiple times because they
  // exists in different scripts. This function only care about unique font
  // names so sort and filter out unique names before returning.
  std::sort(begin(fonts), end(fonts));
  auto last = std::unique(begin(fonts), end(fonts));
  fonts.erase(last, end(fonts));

  return fonts;
}

} // namespace system
} // namespace ruby
