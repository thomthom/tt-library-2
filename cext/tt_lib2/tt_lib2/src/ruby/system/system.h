#pragma once

#include "src/ruby/framework.h"


namespace ruby {
namespace system {


void InitModule(VALUE parent);


} // namespace system
} // namespace ruby
