#include "system.h"

#include "src/ruby/utilities.h"
#include "src/ruby/system/fonts.h"


namespace ruby {
namespace system {
namespace {

VALUE GetHashValue(VALUE hash, const char* symbol, VALUE default_value)
{
  VALUE value = rb_hash_aref(hash, ID2SYM(rb_intern(symbol)));
  return NIL_P(value) ? default_value : value;
}

#pragma warning (push)
#pragma warning (disable : 4100) // warning C4100: unreferenced formal parameter

/**
 * @version 2.11.0
 *
 * @overload font_names(options)
 *   @param [Hash] options
 *   @option options [Boolean] :device (false)
 *   @option options [Boolean] :raster (false)
 *   @option options [Boolean] :vector (true)
 *   @return [Array<String>]
 */
VALUE wrap_font_names(int argc, VALUE* argv, VALUE self)
{
  VALUE options = Qnil;
  rb_scan_args(argc, argv, "01", &options);

  if (NIL_P(options)) options = rb_hash_new();
  Check_Type(options, T_HASH);

  FontOptions font_options {
    RTEST(GetHashValue(options, "device", Qfalse)),
    RTEST(GetHashValue(options, "raster", Qfalse)),
    RTEST(GetHashValue(options, "vector", Qtrue))
  };

  auto fonts = GetFontNames(font_options);
  VALUE names = rb_ary_new_capa(static_cast<long>(fonts.size()));
  for (const auto& font : fonts)
  {
    auto name = GetVALUE(font.c_str());
    rb_ary_push(names, name);
  }
  return names;
}

#pragma warning (pop)

} // namespace


void InitModule(VALUE mTT)
{
  VALUE mSystem = rb_define_module_under(mTT, "System");
  rb_define_module_function(mSystem, "font_names",
      VALUEFUNC(wrap_font_names), -1);
}


} // namespace system
} // namespace ruby
