#include "../fonts.h"

#import <algorithm>

#import <Cocoa/Cocoa.h>


namespace ruby {
namespace system {
namespace {


} // namespace

FontList GetFontNames(const FontOptions& options)
{
  FontList names;

  // https://developer.apple.com/documentation/appkit/nsfontmanager
  NSFontManager* fontManager = [NSFontManager sharedFontManager];
  NSArray* fonts = [fontManager availableFontFamilies];
  for (id font in fonts) {
    names.emplace_back([font UTF8String]);
  }

  std::sort(begin(names), end(names));
  auto last = std::unique(begin(names), end(names));
  names.erase(last, end(names));

  return names;
}

} // namespace system
} // namespace ruby
