#include "point3d.h"

#include "geometry/point3d.h"


namespace tt_lib2 {
namespace ruby {
namespace api {
namespace Geom {


ID iX ;
ID iY;
ID iZ;

ID iNew;

VALUE mGeom;
VALUE cPoint3d;

void Init()
{
    iX = rb_intern("x");
    iY = rb_intern("y");
    iZ = rb_intern("z");
    
    iNew = rb_intern("new");
    
    mGeom    = rb_define_module("Geom");
    cPoint3d = rb_define_class_under(mGeom, "Point3d", rb_cObject);
}


geometry::Point3d VALUE2Point(VALUE ruby_point)
{
	double x = NUM2DBL(rb_funcall(ruby_point, iX, 0));
	double y = NUM2DBL(rb_funcall(ruby_point, iY, 0));
	double z = NUM2DBL(rb_funcall(ruby_point, iZ, 0));
	return geometry::Point3d(x, y, z);
}


VALUE GetVALUE(geometry::Point3d point)
{
	return rb_funcall(cPoint3d, iNew, 3,
		rb_float_new(point.x()),
		rb_float_new(point.y()),
		rb_float_new(point.z())
	);
}


std::vector<geometry::Point3d> VALUE2Points(VALUE ruby_points)
{
	Check_Type(ruby_points, T_ARRAY);
	auto num_ruby_points = RARRAY_LEN(ruby_points);
	std::vector<geometry::Point3d> points;
	points.reserve(num_ruby_points);
	for (long i = 0; i < num_ruby_points; ++i)
  {
    VALUE ruby_point = rb_ary_entry(ruby_points, i);
		auto point = VALUE2Point(ruby_point);
		points.emplace_back(point);
  }
	return points;
}


VALUE GetVALUE(const std::vector<geometry::Point3d>& points)
{
	long num_points = static_cast<long>(points.size());
	VALUE ruby_points = rb_ary_new2(num_points);

	for (const auto& point : points)
	{
		VALUE ruby_point = GetVALUE(point);
    rb_ary_push(ruby_points, ruby_point);
	}
	return ruby_points;
}


} // namespace Geom
} // namespace api
} // namespace ruby
} // namespace tt_lib2
