#include <vector>

#include "ruby/utilities.h"


namespace tt_lib2 {

namespace geometry {
	class Point3d;
}

namespace ruby {
namespace api {
namespace Geom {
    
    
void Init();

geometry::Point3d VALUE2Point(VALUE ruby_point);
VALUE GetVALUE(geometry::Point3d point);

std::vector<geometry::Point3d> VALUE2Points(VALUE ruby_points);
VALUE GetVALUE(const std::vector<geometry::Point3d>& points);


} // namespace Geom
} // namespace api
} // namespace ruby
} // namespace tt_lib2
