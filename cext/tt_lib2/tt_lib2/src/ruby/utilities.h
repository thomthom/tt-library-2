#pragma once

#include <string>

#include "framework.h"


/*
 * Need to be very careful about how these macros are defined, especially
 * when compiling C++ code or C code with an ANSI C compiler.
 *
 * VALUEFUNC(f) is a macro used to typecast a C function that implements
 * a Ruby method so that it can be passed as an argument to API functions
 * like rb_define_method() and rb_define_singleton_method().
 *
 * VOIDFUNC(f) is a macro used to typecast a C function that implements
 * either the "mark" or "free" stuff for a Ruby Data object, so that it
 * can be passed as an argument to API functions like Data_Wrap_Struct()
 * and Data_Make_Struct().
 */

#define VALUEFUNC(f) ((VALUE (*)(ANYARGS)) f)
#define VOIDFUNC(f)  ((RUBY_DATA_FUNC) f)


VALUE GetVALUE(const char* string);
VALUE GetVALUE(const std::string string);

inline VALUE GetVALUE(bool bValue) {
	return bValue ? Qtrue : Qfalse;
}

inline VALUE GetVALUE(double value) {
	// DBL2NUM didn't exist in Ruby 1.8.
	return DBL2NUM(value);
}

inline VALUE GetVALUE(long value) {
	return LONG2NUM(value);
}

inline VALUE GetVALUE(int value) {
	return INT2NUM(value);
}

inline VALUE GetVALUE(unsigned long value) {
	return ULONG2NUM(value);
}

inline VALUE GetVALUE(unsigned int value) {
	return UINT2NUM(value);
}
