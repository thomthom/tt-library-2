#include "ruby/framework.h"
#include "ruby/utilities.h"

#include "ruby/geom3d/geom3d.h"
#include "ruby/system/system.h"

#include "ruby/api/geom/point3d.h"

namespace {

#pragma warning (push)
#pragma warning (disable : 4100) // warning C4100: unreferenced formal parameter

/**
 * @version 2.15.0
 *
 * @param [Object] object
 * @return [Integer]
 */
VALUE wrap_object_address(VALUE self, VALUE object)
{
  static_assert(sizeof(void*) == sizeof(VALUE));
  static_assert(sizeof(void*) == sizeof(size_t));
  auto ptr = reinterpret_cast<void*>(object);
  auto address = reinterpret_cast<size_t>(ptr);
  return ULL2NUM(address);
}

/**
 * Ruby's default `#inspect` implementation without any instance variables.
 * 
 * This is the same as Ruby's C API `rb_any_to_s`.
 * 
 * @version 2.15.0
 *
 * @param [Object] object
 * @return [String]
 */
VALUE wrap_object_inspect(VALUE self, VALUE object)
{
  return rb_any_to_s(object);
}

#pragma warning (pop)

} // namespace


extern "C"
void Init_tt_lib2()
{
  VALUE mTT = rb_define_module("TT");

  // The build version of the TT_Lib binary library.
  rb_define_const(mTT, "CEXT_VERSION", GetVALUE("1.2.0"));

  // The `RUBY_PLATFORM` constant from the Ruby headers.
  rb_define_const(mTT, "CEXT_RUBY_PLATFORM", GetVALUE(RUBY_PLATFORM));

  rb_define_module_function(mTT, "object_address",
    VALUEFUNC(wrap_object_address), 1);

  rb_define_module_function(mTT, "object_inspect",
    VALUEFUNC(wrap_object_inspect), 1);

  ruby::geom3d::InitModule(mTT);
  ruby::system::InitModule(mTT);
  tt_lib2::ruby::api::Geom::Init();
}
