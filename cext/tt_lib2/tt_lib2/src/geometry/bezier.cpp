#include "bezier.h"

#include <assert.h>
#include <cmath>

#include "geometry/point3d.h"


namespace tt_lib2 {
namespace geometry {
namespace bezier {


namespace {

/*
Bezier Blending Function
This function computes the blending function as used in the Bezier surface code
above. It is written for clarity, not efficiency. Normally, if the number of
control points is constant, the blending function would be calculated once for
each desired value of mu.
*/
double BezierBlend(int k, double mu, int n)
{
	double blend = 1.0;

	int nn = n;
	int kn = k;
	int nkn = n - k;

	while (nn >= 1)
	{
		blend *= nn;
		nn--;
		if (kn > 1)
		{
			blend /= static_cast<double>(kn);
			kn--;
		}
		if (nkn > 1)
		{
			blend /= static_cast<double>(nkn);
			nkn--;
		}
	}
	if (k > 0)
		blend *= std::pow(mu, static_cast<double>(k));
	if (n - k > 0)
		blend *= std::pow(1 - mu, static_cast<double>(n - k));

	return blend;
}

double BezierBlend(size_t k, double mu, size_t n)
{
	return BezierBlend(static_cast<int>(k), mu, static_cast<int>(n));
}

} // namespace {


// http://paulbourke.net/geometry/bezier/
/*
 * Control Points are processed as rows and columns where the indexes run in
 * the following pattern:
 *
 * Column
 * ^
 * |
 *
 * 12--13--14--15
 * |   |   |   |
 * 8---9---10--11
 * |   |   |   |
 * 4---5---6---7
 * |   |   |   |
 * 0---1---2---3 ==> Row
 */
std::vector<Point3d> QuadPatch(const std::vector<Point3d>& control_mesh,
	size_t segments)
{
	size_t num_control_mesh_points = static_cast<size_t>(std::sqrt(control_mesh.size()));
	size_t num_control_mesh_segments = num_control_mesh_points - 1;
	
	size_t num_segment_points = segments + 1;
	size_t num_points = num_segment_points * num_segment_points;
	std::vector<Point3d> output = std::vector<Point3d>(num_points);

	for (size_t i = 0; i  < num_segment_points; i++)
	{
		double mui = i / static_cast<double>(num_segment_points - 1);
		for (size_t j = 0; j < num_segment_points; j++)
		{
			double muj = j / static_cast<double>(num_segment_points - 1);
			size_t out_index = (j * num_segment_points) + i;
			assert(out_index < output.size());
			for (size_t ki = 0; ki <= num_control_mesh_segments; ki++)
			{
				double bi = BezierBlend(ki, mui, num_control_mesh_segments);
				for (size_t kj = 0; kj <= num_control_mesh_segments; kj++)
				{
					double bj = BezierBlend(kj, muj, num_control_mesh_segments);
					size_t in_index = (kj * num_control_mesh_points) + ki;
					assert(in_index < control_mesh.size());
					double x = control_mesh[in_index].x() * bi * bj;
					double y = control_mesh[in_index].y() * bi * bj;
					double z = control_mesh[in_index].z() * bi * bj;
					output[out_index] += Point3d(x, y, z);
				}
			}
		}
	}

	return output;
}


} // namespace bezier
} // namespace geometry
} // namespace tt_lib2
