#include "geometry/plane.h"

#include <assert.h>
#include <cmath>

#include "geometry/point3d.h"
#include "geometry/unit_vector3d.h"
#include "geometry/vector3d.h"


namespace tt_lib2 {
namespace geometry {


Plane::Plane(
	const Point3d& point1, const Point3d& point2, const Point3d& point3)
{
	auto vector1 = point2.vector_to(point1);
	auto vector2 = point3.vector_to(point1);
	auto normal = UnitVector3d(vector1 * vector2);

	assert(normal.is_valid());

	a_ = normal.x();
	b_ = normal.y();
	c_ = normal.z();
	d_ = -(point1.x() * a_ + point1.y() * b_ + point1.z() * c_);
}


double Plane::distance_to(const Point3d& point) const
{
	double distance = a_ * point.x() + b_ * point.y() + c_ * point.z() + d_;
	return std::abs(distance);
}


} // namespace geometry
} // namespace tt_lib2
