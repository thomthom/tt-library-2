#include "geometry/unit_vector3d.h"

#include <cmath>

#include "geometry/geometry.h"


namespace tt_lib2 {
namespace geometry {


bool UnitVector3d::normalize()
{
	using namespace geometry;

	double length_squared = (x_ * x_) + (y_ * y_) + (z_ * z_);
	if (length_squared < kFloatToleranceSquared)
	{
    x_ = 0.0;
    y_ = 0.0;
    z_ = 0.0;
    return false;
  }

	if (!IsEqualWithinSquaredTolerance(length_squared, 1.0,
		kFloatToleranceSquared))
	{
    double length_inverted = 1.0 / std::sqrt(length_squared);
    x_ *= length_inverted;
    y_ *= length_inverted;
    z_ *= length_inverted;
  }

  return true;
}


} // namespace geometry
} // namespace tt_lib2
