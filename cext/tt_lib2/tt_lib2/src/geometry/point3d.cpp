#include "geometry/point3d.h"

#include "geometry/geometry.h"
#include "geometry/plane.h"
#include "geometry/vector3d.h"


namespace tt_lib2 {
namespace geometry {


Point3d::Point3d()
{
	x_ = 0.0;
	y_ = 0.0;
	z_ = 0.0;
}

Point3d::Point3d(double x, double y, double z)
{
	x_ = x;
	y_ = y;
	z_ = z;
}


bool Point3d::operator==(const Point3d& point) const
{
	using namespace geometry;

	double dx = x_ - point.x_;
	if (!IsZeroWithinTolerance(dx, kPointTolerance))
	{
		return false;
	}

	double dy = y_ - point.y_;
	if (!IsZeroWithinTolerance(dy, kPointTolerance))
	{
		return false;
	}

	double dz = z_ - point.z_;
	if (!IsZeroWithinTolerance(dz, kPointTolerance))
	{
		return false;
	}

	double dsq = dx * dx + dy * dy + dz * dz;
	return IsZeroWithinSquaredTolerance(dsq, kPointToleranceSquared);
}

bool Point3d::operator!=(const Point3d& point) const
{
	return !operator==(point);
}


Point3d& Point3d::operator+=(const Point3d& point)
{
	x_ += point.x_;
	y_ += point.y_;
	z_ += point.z_;
	return *this;
}

Point3d& Point3d::operator-=(const Point3d& point)
{
	x_ -= point.x_;
	y_ -= point.y_;
	z_ -= point.z_;
	return *this;
}

Point3d& Point3d::operator*=(double scale)
{
	x_ *= scale;
	y_ *= scale;
	z_ *= scale;
	return *this;
}

Point3d& Point3d::operator/=(double inverted_scale)
{
	return *this *= (1.0 / inverted_scale);
}


double Point3d::distance_to(const Plane& plane) const
{
	return plane.distance_to(*this);
}


bool Point3d::is_on(const Plane& plane) const
{
	return plane.distance_to(*this) < geometry::kPointTolerance;
}


Vector3d Point3d::vector_to(const Point3d& point) const
{
	double x = point.x_ - x_;
	double y = point.y_ - y_;
	double z = point.z_ - z_;
	return Vector3d(x, y, z);
}


double Point3d::x() const
{
	return x_;
}

double Point3d::y() const
{
	return y_;
}

double Point3d::z() const
{
	return z_;
}


} // namespace geometry
} // namespace tt_lib2
