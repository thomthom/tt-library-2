#pragma once

#include <ostream>


namespace tt_lib2 {
namespace geometry {


class Plane;
class Vector3d;


class Point3d
{
public:
	Point3d(void);
	Point3d(double x, double y, double z);

	// http://stackoverflow.com/questions/4421706/operator-overloading

	bool operator==(const Point3d& point) const;
	bool operator!=(const Point3d& point) const;

	Point3d& operator+=(const Point3d& point);
	Point3d& operator-=(const Point3d& point);
	Point3d& operator*=(double scale);
	Point3d& operator/=(double inverted_scale);

	double distance_to(const Plane& plane) const;

	bool is_on(const Plane& plane) const;

	Vector3d vector_to(const Point3d& point) const;

	double x() const;
	double y() const;
	double z() const;

private:
	double x_, y_, z_;
};

inline Point3d operator+(Point3d point_left, const Point3d& point_right)
{
	point_left += point_right;
	return point_left;
}

inline Point3d operator-(Point3d point_left, const Point3d& point_right)
{
	point_left -= point_right;
	return point_left;
}

inline Point3d operator*(Point3d point_left, double scale)
{
	point_left *= scale;
	return point_left;
}

inline Point3d operator/(Point3d point_left, double inverted_scale)
{
	point_left /= inverted_scale;
	return point_left;
}

inline ::std::ostream& operator<<(::std::ostream& os, const Point3d& point) {
	return os <<
		"Point3d(" << point.x() << ", " << point.y() << ", " << point.z() << ")";
}


} // namespace geometry
} // namespace tt_lib2
