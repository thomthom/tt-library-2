#pragma once

#include <vector>


namespace tt_lib2 {
namespace geometry {


class Point3d;


const double kFloatTolerance = 1.0e-10;
const double kFloatToleranceSquared = kFloatTolerance * kFloatTolerance;

const double kPointTolerance = 1.0e-3;
const double kPointToleranceSquared = kPointTolerance * kPointTolerance;


Point3d Average(const std::vector<Point3d>& points);
Point3d Average(const Point3d& point1, const Point3d& point2);

Point3d Interpolate(const Point3d& point1, const Point3d& point2,
	double weight_of_first_point);

bool IsEqualWithinTolerance(double number1, double number2, double tolerance);
bool IsEqualWithinSquaredTolerance(double number1, double number2,
	double tolerance);

bool IsEqualOrGreaterWithinTolerance(double number1, double number2,
	double tolerance);
bool IsEqualOrLessWithinTolerance(double number1, double number2,
	double tolerance);

bool IsZeroWithinTolerance(double number, double tolerance);
bool IsZeroWithinSquaredTolerance(double number, double tolerance);


} // namespace geometry
} // namespace tt_lib2
