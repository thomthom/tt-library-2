#include "geometry/geometry.h"

#include <assert.h>
#include <cmath>

#include "geometry/point3d.h"


namespace tt_lib2 {
namespace geometry {


Point3d Average(const std::vector<Point3d>& points)
{
	Point3d average_point = Point3d();
	std::vector<Point3d>::const_iterator it = points.begin();
	for (; it != points.end(); ++it)
	{
		average_point += *it;
	}
	if (points.size() > 0)
	{
		average_point /= static_cast<double>(points.size());
	}
	return average_point;
}

Point3d Average(const Point3d& point1, const Point3d& point2)
{
	Point3d average_point = point1 + point2;
	return average_point /= 2.0;
}

Point3d Interpolate(const Point3d& point1, const Point3d& point2,
	double weight_of_first_point)
{
	assert(weight_of_first_point <= 1.0);
	assert(weight_of_first_point >= 0.0);
	double weight_of_second_point = 1.0 - weight_of_first_point;
	Point3d weighted_point1 = point1 * weight_of_first_point;
	Point3d weighted_point2 = point2 * weight_of_second_point;
	return weighted_point1 + weighted_point2;
}

bool IsEqualWithinTolerance(double number1, double number2,
	double tolerance)
{
	return std::abs(number1 - number2) < tolerance;
}

bool IsEqualWithinSquaredTolerance(double number1, double number2,
	double tolerance)
{
	return std::abs(number1 - number2) < tolerance;
}

bool IsEqualOrGreaterWithinTolerance(double number1, double number2,
	double tolerance)
{
	return number1 > number2 ||
		IsEqualWithinTolerance(number1, number2, tolerance);
}

bool IsEqualOrLessWithinTolerance(double number1, double number2,
	double tolerance)
{
	return number1 < number2 ||
		IsEqualWithinTolerance(number1, number2, tolerance);
}

bool IsZeroWithinTolerance(double number, double tolerance)
{
	return std::abs(number) < tolerance;
}

bool IsZeroWithinSquaredTolerance(double number, double tolerance)
{
	return std::abs(number) < tolerance;
}


} // namespace geometry
} // namespace tt_lib2