#pragma once

#include <ostream>

#include "geometry/geometry.h"


namespace tt_lib2 {
namespace geometry {


class Point3d;


class Vector3d
{
public:
	Vector3d(void) : x_(0.0), y_(0.0), z_(0.0) {}
	Vector3d(double x, double y, double z) : x_(x), y_(y), z_(z) {}
	Vector3d(const Point3d& point);

	// http://stackoverflow.com/questions/4421706/operator-overloading

	bool operator==(const Vector3d& vector) const;
	bool operator!=(const Vector3d& vector) const;

	Vector3d& operator+=(const Vector3d& vector);
	Vector3d& operator-=(const Vector3d& vector);
	Vector3d& operator*=(double scale);
	Vector3d& operator/=(double inverted_scale);

	// Cross product.
	Vector3d operator*(const Vector3d& vector) const;

	bool is_valid() const
	{
		using namespace geometry;
		return !(IsZeroWithinTolerance(x_, kPointTolerance) &&
		         IsZeroWithinTolerance(y_, kPointTolerance) &&
		         IsZeroWithinTolerance(z_, kPointTolerance));
	}

	double x() const;
	double y() const;
	double z() const;

protected:
	double x_, y_, z_;
};

inline Vector3d operator+(Vector3d point_left, const Vector3d& point_right)
{
	point_left += point_right;
	return point_left;
}

inline Vector3d operator-(Vector3d point_left, const Vector3d& point_right)
{
	point_left -= point_right;
	return point_left;
}

inline Vector3d operator*(Vector3d point_left, double scale)
{
	point_left *= scale;
	return point_left;
}

inline Vector3d operator/(Vector3d point_left, double inverted_scale)
{
	point_left /= inverted_scale;
	return point_left;
}

inline ::std::ostream& operator<<(::std::ostream& os, const Vector3d& point) {
	return os <<
		"Vector3d(" << point.x() << ", " << point.y() << ", " << point.z() << ")";
}


} // namespace geometry
} // namespace tt_lib2
