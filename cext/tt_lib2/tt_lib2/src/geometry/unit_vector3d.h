#pragma once

#include <ostream>

#include "geometry/vector3d.h"


namespace tt_lib2 {
namespace geometry {


class Point3d;


class UnitVector3d : public Vector3d
{
public:
	UnitVector3d(void) : Vector3d() {}

	UnitVector3d(double x, double y, double z) : Vector3d(x, y, z)
	{
		normalize();
	}

	UnitVector3d(const Vector3d& vector) :
		Vector3d(vector.x(), vector.y(), vector.z())
	{
		normalize();
	}

	UnitVector3d(const Point3d& point) : Vector3d(point)
	{
		normalize();
	}

private:
	bool normalize();
};


} // namespace geometry
} // namespace tt_lib2
