#include "geometry/vector3d.h"

#include "geometry/point3d.h"


namespace tt_lib2 {
namespace geometry {


Vector3d::Vector3d(const Point3d& point) :
	x_(point.x()), y_(point.y()), z_(point.z()) {}


bool Vector3d::operator==(const Vector3d& vector) const
{
	using namespace geometry;

	double dx = x_ - vector.x_;
	if (!IsZeroWithinTolerance(dx, kPointTolerance))
	{
		return false;
	}

	double dy = y_ - vector.y_;
	if (!IsZeroWithinTolerance(dy, kPointTolerance))
	{
		return false;
	}

	double dz = z_ - vector.z_;
	if (!IsZeroWithinTolerance(dz, kPointTolerance))
	{
		return false;
	}

	double dsq = dx * dx + dy * dy + dz * dz;
	return IsZeroWithinSquaredTolerance(dsq, kPointToleranceSquared);
}

bool Vector3d::operator!=(const Vector3d& vector) const
{
	return !operator==(vector);
}


Vector3d& Vector3d::operator+=(const Vector3d& vector)
{
	x_ += vector.x_;
	y_ += vector.y_;
	z_ += vector.z_;
	return *this;
}

Vector3d& Vector3d::operator-=(const Vector3d& vector)
{
	x_ -= vector.x_;
	y_ -= vector.y_;
	z_ -= vector.z_;
	return *this;
}

Vector3d& Vector3d::operator*=(double scale)
{
	x_ *= scale;
	y_ *= scale;
	z_ *= scale;
	return *this;
}

Vector3d& Vector3d::operator/=(double inverted_scale)
{
	return *this *= (1.0 / inverted_scale);
}


Vector3d Vector3d::operator*(const Vector3d& vector) const
{
	double x = (y_ * vector.z_) - (z_ * vector.y_);
  double y = (z_ * vector.x_) - (x_ * vector.z_);
  double z = (x_ * vector.y_) - (y_ * vector.x_);
  return Vector3d(x, y, z);
}


double Vector3d::x() const
{
	return x_;
}

double Vector3d::y() const
{
	return y_;
}

double Vector3d::z() const
{
	return z_;
}


} // namespace geometry
} // namespace tt_lib2
