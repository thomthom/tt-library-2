#pragma once


namespace tt_lib2 {
namespace geometry {


class Point3d;


class Plane
{
public:
	Plane(void) : a_(0.0), b_(0.0), c_(1.0), d_(0.0) {}

	Plane(const Point3d& point1, const Point3d& point2, const Point3d& point3);

	double distance_to(const Point3d& point) const;

	double a() const
	{
		return a_;
	}

	double b() const
	{
		return b_;
	}

	double c() const
	{
		return c_;
	}

	double d() const
	{
		return d_;
	}

private:
	double a_, b_, c_, d_;
};


} // namespace geometry
} // namespace tt_lib2
