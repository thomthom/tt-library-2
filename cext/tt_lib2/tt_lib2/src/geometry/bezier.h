#include <vector>


namespace tt_lib2 {
namespace geometry {

class Point3d;

namespace bezier {


std::vector<Point3d> QuadPatch(const std::vector<Point3d>& control_mesh,
	size_t segments);


} // namespace bezier
} // namespace geometry
} // namespace tt_lib2
