class TestTool

  def initialize
    path= File.dirname( __FILE__ )
    gl_file = File.join( path, 'test.gli' )
    @gl = TT::GL_Image.new( gl_file )
  end
  
  def onMouseMove( flags, x, y, view )
    view.invalidate
  end
  
  def draw(view)
    @gl.draw( view, 100, 200 )
  end
  
end # class