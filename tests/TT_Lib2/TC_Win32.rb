#-------------------------------------------------------------------------------
#
# Thomas Thomassen
# thomas[at]thomthom[dot]net
#
#-------------------------------------------------------------------------------

if Sketchup.platform == :platform_win

require 'testup/testcase'

module TT::Lib
module Tests

  class TC_Win32 < TestUp::TestCase

    def setup
      # ...
    end

    def teardown
      # ...
    end

    # Later versions of Fiddle returns a pointer for HWND return values.
    # Fiddle::Pointer implicitly converts to an Integer so in most cases they
    # are interchangeable for what TT_Lib do.
    HWND_TYPE = RUBY_VERSION.to_i < 3 ? Integer : Fiddle::Pointer

    # ======================================================================== #
    # method TT::Win32.get_sketchup_window

    def test_get_sketchup_window
      dialog = UI::WebDialog.new
      dialog.show # Ensure the main window doesn't have focus.
      hwnd = TT::Win32.get_sketchup_window
      assert_kind_of(HWND_TYPE, hwnd)
      refute_equal(0, hwnd)
    ensure
      dialog.close
    end

    # ======================================================================== #
    # method TT::Win32.activate_sketchup_window

    def test_activate_sketchup_window
      dialog = UI::WebDialog.new
      dialog.show # Ensure the main window doesn't have focus.
      hwnd = TT::Win32.activate_sketchup_window
      assert_kind_of(HWND_TYPE, hwnd)
      refute_equal(0, hwnd)
    ensure
      dialog.close
    end

    # ======================================================================== #
    # method TT::Win32.get_window_text

    def test_get_window_text
      dialog_title = 'TT_Lib Test'
      dialog = UI::WebDialog.new(dialog_title: dialog_title)
      dialog.show

      # TT_Lib doesn't have a method to get the active dialog title. Don't
      # want to add that just for the test. For now fetching the SketchUp
      # windows and just checking it has "SketchUp" in it.
      hwnd = TT::Win32.get_sketchup_window
      assert_kind_of(HWND_TYPE, hwnd)
      refute_equal(0, hwnd)

      result = TT::Win32.get_window_text(hwnd)
      assert_kind_of(String, result)
      # Sketchup.app_name should normally return "SketchUp Pro" for pro
      # versions of SketchUp, but some times SketchUp drops the "Pro".
      # assert_includes(result, Sketchup.app_name)
      assert_includes(result, 'SketchUp')
    ensure
      dialog.close
    end

    # TODO: Test failure?

    # ======================================================================== #
    # method TT::Win32.get_folder_path

    def test_get_folder_path
      result = TT::Win32.get_folder_path(TT::Win32::CSIDL_LOCAL_APPDATA)
      assert_kind_of(String, result)
      refute_empty(result)
      assert_equal(Encoding::UTF_8, result.encoding)
    end

    # ======================================================================== #
    # method TT::Win32.get_folder_path_ansi

    def test_get_folder_path_ansi
      result = TT::Win32.get_folder_path_ansi(TT::Win32::CSIDL_LOCAL_APPDATA)
      assert_kind_of(String, result)
      refute_empty(result)
      assert_equal(Encoding::UTF_8, result.encoding)
    end

    # ======================================================================== #
    # method TT::Win32.get_short_folder_path

    def test_get_short_folder_path
      result = TT::Win32.get_short_folder_path(TT::Win32::CSIDL_LOCAL_APPDATA)
      assert_kind_of(String, result)
      refute_empty(result)
      assert_equal(Encoding::UTF_8, result.encoding)
    end

    # ======================================================================== #
    # method TT::Win32.get_short_folder_path_ansi

    def test_get_short_folder_path_ansi
      result = TT::Win32.get_short_folder_path_ansi(TT::Win32::CSIDL_LOCAL_APPDATA)
      assert_kind_of(String, result)
      refute_empty(result)
      assert_equal(Encoding::UTF_8, result.encoding)
    end

    # ======================================================================== #
    # method TT::Win32.get_folder_path_utf16

    def test_get_folder_path_utf16
      result = TT::Win32.get_folder_path_utf16(TT::Win32::CSIDL_LOCAL_APPDATA)
      assert_kind_of(String, result)
      refute_empty(result)
      # TODO: Ruby 1.8 didn't have string encoding. As of Ruby 2.x this still
      #   returned UTF-8 encoding, even though the data was UTF-16.
      # assert_equal(Encoding::UTF_16LE, result.encoding)
    end

    # ======================================================================== #
    # method TT::Win32.get_short_folder_path_utf16

    def test_get_short_folder_path_utf16
      result = TT::Win32.get_short_folder_path_utf16(TT::Win32::CSIDL_LOCAL_APPDATA)
      assert_kind_of(String, result)
      refute_empty(result)
      # TODO: Ruby 1.8 didn't have string encoding. As of Ruby 2.x this still
      #   returned UTF-8 encoding, even though the data was UTF-16.
      # assert_equal(Encoding::UTF_16LE, result.encoding)
    end

    # ======================================================================== #
    # method TT::Win32.utf8_to_utf16

    def test_utf8_to_utf16
      utf8 = 'Hello Wørld'
      assert_equal(Encoding::UTF_8, utf8.encoding)
      result = TT::Win32.utf8_to_utf16(utf8)
      assert_kind_of(String, result)
      refute_empty(result)
      assert(result.bytesize > utf8.bytesize, 'Bytesize')
      # TODO: Ruby 1.8 didn't have string encoding. As of Ruby 2.x this still
      #   returned UTF-8 encoding, even though the data was UTF-16.
      # assert_equal(utf8.size, result.size, 'Size')
      # assert_equal(Encoding::UTF_16LE, result.encoding)
      expected_size = (utf8.size + 1) * 2
      assert_equal(expected_size, result.bytesize, "Size: #{result.inspect}")
      assert_equal(Encoding::UTF_8, result.encoding)
      expected_bytes = [72, 0, 101, 0, 108, 0, 108, 0, 111, 0, 32, 0, 87, 0, 248, 0, 114, 0, 108, 0, 100, 0, 0, 0]
      assert_equal(expected_bytes, result.bytes)
    end

    # ======================================================================== #
    # method TT::Win32.utf16_to_ansi

    def test_utf16_to_ansi
      utf8 = 'Hello Wørld'
      utf16 = TT::Win32.utf8_to_utf16(utf8)
      assert_equal(Encoding::UTF_8, utf8.encoding)
      result = TT::Win32.utf16_to_ansi(utf16)
      assert_kind_of(String, result)
      refute_empty(result)
      assert(result.bytesize < utf16.bytesize, 'Bytesize')
      # TODO: Ruby 1.8 didn't have string encoding. As of Ruby 2.x this still
      #   returned UTF-8 encoding, even though the data was ANSI.
      # assert_equal(utf8.size, result.size, 'Size')
      # assert_equal(Encoding::US_ASCII, result.encoding)
      assert_equal(utf8.size, result.size, 'Size')
      assert_equal(Encoding::UTF_8, result.encoding)
      expected_bytes = [72, 101, 108, 108, 111, 32, 87, 248, 114, 108, 100]
      assert_equal(expected_bytes, result.bytes)
    end

    # ======================================================================== #
    # method TT::Win32.utf16_to_utf8

    def test_utf16_to_utf8
      utf8 = 'Hello Wørld'
      utf16 = TT::Win32.utf8_to_utf16(utf8)
      assert_equal(Encoding::UTF_8, utf8.encoding)
      result = TT::Win32.utf16_to_utf8(utf16)
      assert_kind_of(String, result)
      refute_empty(result)
      assert(result.bytesize < utf16.bytesize, 'Bytesize')
      # This shouldn't really need the +1 to the expected size. But quirks in
      # the original implementation lead to this due to how Ruby and string
      # encoding works with string manipulations.
      assert_equal(utf8.size + 1, result.size, "Size: #{result.inspect}")
      # TODO: Ruby 1.8 didn't have string encoding. As of Ruby 2.x this still
      #   returned UTF-8 encoding, even though the data was ANSI.
      assert_equal(Encoding::UTF_8, result.encoding)
      assert_equal(utf8, result)
      expected_bytes = [72, 101, 108, 108, 111, 32, 87, 195, 184, 114, 108, 100]
      assert_equal(expected_bytes, result.bytes)
    end

    # ======================================================================== #
    # method TT::Win32.make_toolwindow_frame

    def test_make_toolwindow_frame
      dialog_title = 'TT_Lib Test'
      dialog = UI::WebDialog.new(dialog_title: dialog_title)
      dialog.show
      result = TT::Win32.make_toolwindow_frame(dialog_title)
      assert_kind_of(TrueClass, result)
    ensure
      dialog.close
    end

    def test_make_toolwindow_frame_invalid_name
      result = TT::Win32.make_toolwindow_frame('fake')
      assert_nil(result)
    end

    # ======================================================================== #
    # method TT::Win32.window_no_resize

    def test_window_no_resize
      dialog_title = 'TT_Lib Test'
      dialog = UI::WebDialog.new(dialog_title: dialog_title)
      dialog.show
      result = TT::Win32.window_no_resize(dialog_title)
      assert_kind_of(TrueClass, result)
    ensure
      dialog.close
    end

    def test_window_no_resize_invalid_name
      result = TT::Win32.window_no_resize('fake')
      assert_nil(result)
    end

    # ======================================================================== #
    # method TT::Win32.refresh_sketchup

    def test_refresh_sketchup
      result = TT::Win32.refresh_sketchup
      assert_kind_of(FalseClass, result)
    end

  end # TestCase

end # Tests
end # module TT::Lib

end # if Windows
